Received an application from {{last_name}} {{first_name}} at {{apply_time}}

Applier Information:

    {
        "email": {{applier_email}},
        "first_name": "{{first_name}}",
        "last_name": "{{last_name}}"',
        "contact_number": "{{phone}}",
        "title": "{{title}}",
        "content": "{{content}}",
        "link": "{{link}}"
    }