__author__ = 'PyBeaner'

from tornado.web import RequestHandler


class BasicRequestHandler(RequestHandler):
    def _post(self):
        try:
            args = self.getArguments()
            assert isinstance(args, dict)
            try:
                self.realAction(kwargs=args)
            except Exception,e:
                self.write('<script>alert("%s")</script>' % str(e))
        except ValueError,e:
            self.write('invalid argument')

    def getArguments(self):
        kws = {}
        rest_args = self.getRestfulArgs()
        if not rest_args:
            return kws
        assert isinstance(rest_args, list)
        for arg in rest_args:
            length = len(arg)
            # has a default value
            if length == 3:
                value = self.get_argument(arg[0], arg[2])
            else:
                value = self.get_argument(arg[0])
            # type conversion
            if length == 2 and arg[1]:
                value = arg[1](self.get_argument(arg[0]))
            kws[arg[0]] = value
        return kws

    def getRestfulArgs(self):
        """
        return a list of restful arguments:
                [
                ('name', 'convert_to', 'default_value'),
                ('age', int, 0),
                ]
        """
        return []

    def realAction(self, kwargs):
        """
        real action to deal with post/get
        """
        pass
