__author__ = 'PyBeaner'
import tornado.web
import tornado.httpserver
import tornado.ioloop
import os
from rest import *

SITE_ROOT = os.path.dirname(__file__)
settings = {
    "template_path": os.path.join(SITE_ROOT,'templates'),
    "static_path": os.path.join(SITE_ROOT,'static'),
}

application = tornado.web.Application([
    ('/rest/?', RestHandler),
], **settings)

server = tornado.httpserver.HTTPServer(application)
server.listen(8888)
tornado.ioloop.IOLoop.instance().start()