通过main.py运行server（tornado），因此首先需要安装依赖:pip install tornado

测试地址:http://localhost:8888/rest/

页面显示一个简单的申请表单，包括所有需求中提到的字段

邮箱配置在rest.py中：
    MAIL_SERVER，MAIL_USER，MAIL_PASSWORD分别指定测试邮件服务器的地址，账号，密码
    DEDICATED_MAIL即收到申请通知的专用账号
    
邮箱地址在前端通过Email input限制，在后台通过正则判断
手机号码做了中国常用号码段限制
