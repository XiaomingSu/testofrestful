__author__ = 'PyBeaner'
from basic import BasicRequestHandler
MAIL_SERVER = 'smtp.163.com'
MAIL_USER = 'acttao_suxiaoming@163.com'
MAIL_PASSWORD = 'testtest'
DEDICATED_MAIL = 'acttao_suxiaoming@163.com'

class RestHandler(BasicRequestHandler):
    def get(self):
        self.render('apply.html')

    def post(self):
        self._post()

    def realAction(self, kwargs):
        print 'submitting...'
        print kwargs
        self.onSubmit(**kwargs)
        applier_mail_body = self.render_string('apply_success.txt', last_name=kwargs['last_name'])
        import datetime
        master_body = self.render_string(
            'info_master.txt',
            first_name=kwargs['first_name'],
            last_name=kwargs['last_name'],
            applier_email=kwargs['email'],
            phone=kwargs['phone'],
            title=kwargs['title'],
            content=kwargs['content'],
            link=kwargs['link'],
            apply_time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        )
        print applier_mail_body
        self.send_mail(to_mail=kwargs['email'], subject='Thanks for your application', body=applier_mail_body)
        print master_body
        self.send_mail(to_mail=DEDICATED_MAIL, subject='Application Received from '+kwargs['email'], body=master_body)
        self.write('<script>alert("apply successfully!")</script>')

    def getRestfulArgs(self):
        return [
            ('email', str, ''),
            ('first_name', str, ''),
            ('last_name', str, ''),
            ('phone', int, 0),
            ('title', str, ''),
            ('content', str, ''),
            ('link', str, ''),
        ]

    def onSubmit(self, email, first_name, last_name, phone, title, content, link):
        print '...'
        if not (email and phone and title and content and first_name and last_name and link):
            raise Exception('information incomplete')
        import re
        email_regex = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
        if not re.match(email_regex, email):
            raise Exception('email invalid')
        phone_regex = re.compile(
            "^(13|14|15|17|18)[0-9]{9}$")
        if not re.match(phone_regex, phone):
            raise Exception('phone invalid')

    def send_mail(self, to_mail, subject, body):
        import smtplib
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText

        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = 'acttao_suxiaoming@163.com'
        msg['To'] = to_mail
        body = MIMEText(body, 'plain')
        msg.attach(body)
        s = smtplib.SMTP()
        s.connect(MAIL_SERVER)
        s.login(MAIL_USER, MAIL_PASSWORD)
        s.sendmail(MAIL_USER, to_mail, msg.as_string())
        s.quit()
